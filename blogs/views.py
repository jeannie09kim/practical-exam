from datetime import datetime

from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson as json

from blogs.models import FeedHit, Post, Section
from blogs.settings import ALL_SECTION_NAME
from blogs.signals import post_viewed
from blogs.forms import ContactForm, SignUpForm, LoginForm, AdminPostForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail


@login_required(login_url='/blogs/login/')
def blog_index(request):
    
    sections = Section.objects.all()
    posts = Post.objects.current()
    
    return render_to_response("blogs/blog_list.html", {
        "posts": posts,
        "sections": sections,
    }, context_instance=RequestContext(request))

@login_required(login_url='/blogs/login/')
def blog_section_list(request, section_slug):
    
    sections = Section.objects.all()
    section = get_object_or_404(Section, slug=section_slug)
    posts   = section.posts.all()
    return render_to_response("blogs/blog_section_list.html", {
        "section_slug": section_slug,
        "section_name": section.name,
        "posts": posts,
        "sections": sections,
    }, context_instance=RequestContext(request))

@login_required(login_url='/blogs/login/')
def blog_post_detail(request, **kwargs):
    
    sections = Section.objects.all()
    if "post_pk" in kwargs:
        if request.user.is_authenticated() and request.user.is_staff:
            queryset = Post.objects.all()
            post = get_object_or_404(queryset, pk=kwargs["post_pk"])
        else:
            raise Http404()
    else:
        queryset = Post.objects.current()
        queryset = queryset.filter(
            published__year = int(kwargs["year"]),
            published__month = int(kwargs["month"]),
            published__day = int(kwargs["day"]),
        )
        post = get_object_or_404(queryset, slug=kwargs["slug"])
        post_viewed.send(sender=post, post=post, request=request)
    
    return render_to_response("blogs/blog_post.html", {
        "post": post,
        "sections": sections,
    }, context_instance=RequestContext(request))

@login_required(login_url='/blogs/login/')
def serialize_request(request):
    data = {
        "path": request.path,
        "META": {
            "QUERY_STRING": request.META.get("QUERY_STRING"),
            "REMOTE_ADDR": request.META.get("REMOTE_ADDR"),
        }
    }
    for key in request.META:
        if key.startswith("HTTP"):
            data["META"][key] = request.META[key]
    return json.dumps(data)

@login_required(login_url='/blogs/login/')
def blog_feed(request, section=None):
    
    section = get_object_or_404(Section, slug=section)
    posts = Post.objects.filter(section=section)

    if section is None:
        section = Section.objects.values_list('name', flat=True).order_by('name')

    current_site = Site.objects.get_current()
    
    feed_title = "%s Blog: %s" % (current_site.name, section[0].upper() + section[1:])
    
    blog_url = "http://%s%s" % (current_site.domain, reverse("blog"))
    
    url_name, kwargs = "blog_feed", {"section": section}
    feed_url = "http://%s%s" % (current_site.domain, reverse(url_name, kwargs=kwargs))
    
    if posts:
        feed_updated = posts[0].published
    else:
        feed_updated = datetime(2009, 8, 1, 0, 0, 0)
    
    # create a feed hit
    hit = FeedHit()
    hit.request_data = serialize_request(request)
    hit.save()
    
    atom = render_to_string("blogs/atom_feed.xml", {
        "feed_id": feed_url,
        "feed_title": feed_title,
        "blog_url": blog_url,
        "feed_url": feed_url,
        "feed_updated": feed_updated,
        "entries": posts,
        "current_site": current_site,
    })
    return HttpResponse(atom, mimetype="application/atom+xml")

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            email = form.cleaned_data['email']
     
            send_mail(subject, message, email, ['jrvjaralve@addu.edu.ph'], fail_silently=True)
            
            
            return HttpResponseRedirect('/')
        else:
            ContactForm(request.POST)
    else:
        form = ContactForm()

    return render(request, 'blogs/blog_contact.html', { 'form': form,}) 

def signup(request):
    registered = False

    if request.method == 'POST':
        form = SignUpForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()

            registered = True
        else:
            print form.errors
    else:
        form = SignUpForm()

    return render(request, 'blogs/signup.html', { 'form': form, 'registered':registered,})

def login_view(request):
    context = {}
    context['check_errors'] = False
     
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
     
            user = authenticate(username=username, password=password)
     
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    context['check_errors'] = True
                    pass
            else:
                context['check_errors'] = True
                pass
     
            return HttpResponseRedirect('/')
    else:
        form = LoginForm()
     
    return render(request, 'blogs/login.html', { 'form': form,})

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')

def add_post(request):
    if request.method == 'POST':
        form = AdminPostForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            AdminPostForm(request.POST)
    else:
        form = AdminPostForm()

    return render(request, 'blogs/add_post.html', { 'form': form,}, context_instance=RequestContext(request))

def blog_manage_post(request):

    sections = Section.objects.all()
    posts = Post.objects.filter(author=request.user).filter()

    return render_to_response('blogs/blog_manage_post.html', { 'posts': posts, 'sections':sections,}, context_instance=RequestContext(request))


def blog_update_post(request,post_pk): 
    
    sections = Section.objects.all()
    post = Post.objects.get(pk=post_pk) 
    if request.method == 'POST': 
        update = AdminPostForm(request.POST) 

        if update.is_valid(): 
            update.save() 
            return HttpResponseRedirect('/')
        else: 
            AdminPostForm(request.POST) 

    else: 
        if post: 
            update = AdminPostForm(instance=post) 

        else: 
            update = AdminPostForm() 
     
    return render_to_response("blogs/blog_update_post.html", {'update':update,'post':post, 'sections':sections,}, context_instance=RequestContext(request)) 

def blog_delete_post(request,post_pk): 

    sections = Section.objects.all()
    post = Post.objects.get(pk=post_pk) 
    if post: 
        post.delete() 
    else: 
        pass 

    posts = Post.objects.current() 
    
    return render_to_response("blogs/blog_manage_post.html", {'posts':posts, 'sections':sections,}, context_instance=RequestContext(request)) 