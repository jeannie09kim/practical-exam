from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin

urlpatterns = patterns(
    "",
    url(r"^admin/", include(admin.site.urls)),
    url(r"", include("blogs.urls")),
    url(r'^blogs/contact_us/', "blogs.views.contact", name="contact_us"),
    url(r'^blogs/signup/$', "blogs.views.signup", name="signup"),
    url(r'^blogs/login/$', "blogs.views.login_view", name="login"),
    url(r'^blogs/logout/$', "blogs.views.logout_view", name="logout"), 
    url(r'^blogs/add_post/$', "blogs.views.add_post", name="add_post"),
    url(r'^blogs/blog_manage_post/$', "blogs.views.blog_manage_post", name="blog_manage_post"),
    url(r'^blogs/blog_update_post/(?P<post_pk>\d+)/$', "blogs.views.blog_update_post", name="blog_update_post"),
    url(r'^blogs/blog_delete_post/(?P<post_pk>\d+)/$', "blogs.views.blog_delete_post", name="blog_delete_post"),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
